import pandas as pd
from sklearn.preprocessing import StandardScaler, PowerTransformer

scalers = {
    "standard": StandardScaler(),
    "boxcox": PowerTransformer(),
}

df = pd.read_csv(snakemake.input.dataset)
df = df.dropna()

df_test = df.sample(
    frac=snakemake.params.test_size, random_state=snakemake.params.random_seed
)
df_train = df.copy()[~df.index.isin(df_test.index)]

scaler = scalers.get(snakemake.params.scaler, StandardScaler())
df_train[df_train.columns[:-1]] = scaler.fit_transform(
    df_train[df_train.columns[:-1]]
)
df_test[df_test.columns[:-1]] = scaler.transform(df_test[df_test.columns[:-1]])

df_train.to_csv(snakemake.output.train, index=False)
df_test.to_csv(snakemake.output.test, index=False)
