# MLops homework: Snakemake

This is a homework repository for the [MLOps & production for data science research 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

A reproducible Snakemake pipeline. See docs/dag.md for the graph and description.

The report is also deployed at at https://homework-04-mlops3421713-fa6b5340f492a3b767402aeedb3dae0f211ab2.gitlab.io/


## Running:

`conda env create -f dev.yaml`

`conda run -n dev snakemake --cores all`
